import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {ManualComponent} from './manual/manual.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children:
    [
      {
        path: 'manual',
        component: ManualComponent
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {ManualService} from '../../service/manualService';
import {SujetService} from "../../service/sujetService";
import {EtapeService} from "../../service/etapeService";
import {UploadFileService} from "../../service/UploadFileService";
import {HttpResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {ProfileComponent} from "../profile/profile.component";

@Component({
  selector: 'app-manual',
  templateUrl: './manual.component.html',
  styleUrls: ['./manual.component.css']
})
export class ManualComponent implements OnInit {

  public titre;
  public desc;
  public id;
  public sujets;
  public etapes;
  public id_sujet;
  selectedFiles: FileList;
  currentFileUpload: File;
  constructor(public m: ManualService, public s:SujetService , public e:EtapeService,private uploadService: UploadFileService) { }

  ngOnInit() {
    this.getmanual();
    this.getsujet();
    console.log(ProfileComponent.id_projet);
  }

  ajouterManual(value: any) {
    console.log(value);
    this.m.createManual(value)
      .subscribe(manual => this.m.associateManual('http://localhost:8080/manuel_util/' + manual.id,ProfileComponent.id_projet).subscribe() );
  }
  getmanual() {
    this.m.getManual(ProfileComponent.id_projet).subscribe(manual => {this.titre = manual.titre , this.desc = manual.desc , this.id = manual.id; });
  }

  ajouterSujet(value: any) {
    console.log(value);
    this.s.createSujet(value)
      .subscribe(sujet => this.s.associateSujet('http://localhost:8080/manuel_util/' + this.id, sujet.id).subscribe(s => this.getsujet()) );

  }
  getsujet() {
    this.s.getSujet().subscribe(manual => this.sujets = manual._embedded.sujet);
  }


  ajouterEtape(value: any) {
    console.log(value);
    this.e.createEtape(value)
      .subscribe(sujet => this.e.associateEtape('http://localhost:8080/sujet/' + this.id_sujet, sujet.id).subscribe(e => this.getetape(this.id_sujet)) );

  }
  getetape(id:any) {
    this.id_sujet = id;
    this.etapes = [];
    this.e.getEtape(id).subscribe(manual => {
      this.etapes = manual._embedded.etape
      console.log(this.etapes);
      this.etapes = this.etapes.sort((a, b) => a.id < b.id ? -1 : 1);
      console.log(this.etapes);

    });
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }
  upload() {
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
      }
    });
    this.selectedFiles = undefined;
  }
  split(path:String):String{
    const splitted = path.split('\\');
    return splitted[splitted.length - 1];
  }
}

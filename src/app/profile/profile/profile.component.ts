import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  static id_projet;
  constructor(private activeRoute: ActivatedRoute) {}
  ngOnInit() {
    this.activeRoute.params.subscribe( params => ProfileComponent.id_projet = params.id );
  }

}

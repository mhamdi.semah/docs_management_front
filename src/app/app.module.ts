import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ProfileModule} from './profile/profile.module';
import {ManualService} from './service/manualService';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {SujetService} from './service/sujetService';
import {EtapeService} from "./service/etapeService";
import {UploadFileService} from "./service/UploadFileService";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ManualService,SujetService,EtapeService,UploadFileService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'text/uri-list',
    'Access-Control-Allow-Origin': '/*'
  })
};
@Injectable()
export class SujetService {
  constructor(public http: HttpClient) { }

  public createSujet(sujet: any): Observable<any> {
    return this.http
      .post('http://localhost:8080/sujet', sujet)
      .pipe(catchError(this.handleError)
      );
  }
  public getSujet(): Observable<any> {
    return this.http
      .get('http://localhost:8080/manuel_util/11/sujets')
      .pipe(catchError(this.handleError)
      );
  }
  public associateSujet(manual: any, id: any): Observable<any> {
    return this.http
      .put('http://localhost:8080/sujet/' + id + '/manuel_util', manual,httpOptions)
      .pipe(catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
